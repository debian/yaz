Source: yaz
Section: utils
Standards-Version: 4.7.0
Maintainer: Vincent Danjean <vdanjean@debian.org>
Uploaders: Hugh McMaster <hmc@debian.org>
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 autoconf,
 automake,
 bison,
 docbook-dsssl,
 docbook-xml,
 docbook-xsl,
 gawk,
 libgnutls28-dev,
 libicu-dev,
 libpcap0.8-dev,
 libreadline-dev,
 libtool,
 libwrap0-dev,
 libxslt1-dev,
 pkgconf,
 tcl,
 xsltproc
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/debian/yaz
Vcs-Git: https://salsa.debian.org/debian/yaz.git
Homepage: https://www.indexdata.com/resources/software/yaz/

Package: libyaz5
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Z39.50/SRW/SRU toolkit (libraries)
 YAZ is a toolkit for the development of client and server software built
 around the ANSI Z39.50 (ISO 23950) standard for information retrieval.
 .
 YAZ supports Z39.50-2003 (version 3), SRU versions 1.1 to 2.0, and the
 industry standard ZOOM API for Z39.50.
 .
 This package provides the yaz, yaz-icu and yaz-server libraries.

Package: yaz
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Conflicts: yaz-runtime, yaz-ssl
Description: Z39.50/SRW/SRU toolkit (utilities)
 YAZ is a toolkit for the development of client and server software built
 around the ANSI Z39.50 (ISO 23950) standard for information retrieval.
 .
 YAZ supports Z39.50-2003 (version 3), SRU versions 1.1 to 2.0, and the
 industry standard ZOOM API for Z39.50.
 .
 This package provides several utility programs for retrieving and working
 with data records obtained from URLs and web services.

Package: yaz-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: Z39.50/SRW/SRU toolkit (documentation)
 YAZ is a toolkit for the development of client and server software built
 around the ANSI Z39.50 (ISO 23950) standard for information retrieval.
 .
 YAZ supports Z39.50-2003 (version 3), SRU versions 1.1 to 2.0, and the
 industry standard ZOOM API for Z39.50.
 .
 This package provides HTML documentation for YAZ.

Package: libyaz-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libicu-dev,
 libgnutls28-dev,
 libwrap0-dev,
 libxslt1-dev,
 libyaz5 (= ${binary:Version}),
 ${misc:Depends}
Recommends: tcl
Suggests: yaz-doc
Description: Z39.50/SRW/SRU toolkit (development files)
 YAZ is a toolkit for the development of client and server software built
 around the ANSI Z39.50 (ISO 23950) standard for information retrieval.
 .
 YAZ supports Z39.50-2003 (version 3), SRU versions 1.1 to 2.0, and the
 industry standard ZOOM API for Z39.50.
 .
 This package includes development libraries and C/C++ header files.

Package: yaz-illclient
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Z39.50/SRW/SRU toolkit (command-line client for ILL)
 YAZ is a toolkit for the development of client and server software built
 around the ANSI Z39.50 (ISO 23950) standard for information retrieval.
 .
 YAZ supports Z39.50-2003 (version 3), SRU versions 1.1 to 2.0, and the
 industry standard ZOOM API for Z39.50.
 .
 The yaz-illclient program is a command-line client that sends ISO ILL
 requests to a server and decodes the response.

Package: yaz-icu
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Z39.50/SRW/SRU toolkit (command-line client for ICU)
 YAZ is a toolkit for the development of client and server software built
 around the ANSI Z39.50 (ISO 23950) standard for information retrieval.
 .
 YAZ supports Z39.50-2003 (version 3), SRU versions 1.1 to 2.0, and the
 industry standard ZOOM API for Z39.50.
 .
 The yaz-icu program is a command-line client that exposes the ICU chain
 module of YAZ.
